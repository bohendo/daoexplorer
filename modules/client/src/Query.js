import Buffer from 'buffer';
import { ethers } from 'ethers';

let stakes = `
stakes {
  staker {
    address
  }
  stakeAmount
  prediction
}
`

let votes = `
votes {
  voter {
    address
  }
  voteOption
}
`

let redemptions = `
redemptions {
  proposal {
    proposalId
  }
  account {
    address
  }
  type
  amount
}
`

let common = `
  proposalId
  proposer {
    address
  }
  dao {
    avatarAddress
  }
  state
  decision
`
const q = {}

q.getAllProposals = () => {
  return `
    query { 
      proposals { 
        proposalId
      decision 
      }
    }
  `
}

q.getAllProposalsWithDetails = () => {
  return `
    query {
      proposals {
        ${common}
        ${stakes}
        ${votes}
        ${redemptions}
      }
    }
  `
}

q.getAllProposalsFor = (avatar) => {
  return `
    query {
      dao (id: "${avatar}") {
        proposals {
          proposalId
          decision
        }
      }
    }
  `
}

q.getAllProposalsWithDetailsFor = (avatar) => {
  return `
    query {
      dao (id: "${avatar}") {
        proposals {
          ${common}
          ${stakes}
          ${votes}
          ${redemptions}
        }
      }
    }
  `
}

q.getAllProposalsProposedBy = (proposer) => {
  return `
    query {
      accounts ( where: { address: "${proposer}" }) {
        proposals {
          ${common}
          ${stakes}
          ${votes}
          ${redemptions}
        }
      }
    }
  `
}

q.getAllProposalsVotedBy = (voter) => {
  return `
    query {
      accounts ( where: { address: "${voter}" } )
      votes {
        proposal {
          ${common}
        }
      }
    }
  `
}

q.getAllProposalsStakedBy = (staker) => {
  return `
    query {
      accounts ( where: { address: "${staker}" } )
      stakes {
        proposal {
          ${common}
        }
      }
    }
  `
}

q.getDetailsOfProposal = (id) => {
  return `
    query {
      proposal( id: "${id}" ) {
        ${common}
        ${stakes}
        ${votes}
        ${redemptions}
      }
    }
  `
}

q.getAllRewardsFor = (beneficiary) => {
  return `
    query {
      accounts ( where: { address: "${beneficiary}" }) {
        ${redemptions}
      }
    }
  `
}

q.getAllUsers = (avatar) => {
  return `
    query {
      dao ( id: "${avatar}" ) {
        accounts {
          address
        }
      }
    }
  `
}

q.getAllDetailsForUser = (user, avatar) => {
  console.log(`executing query to get user ${user} with ethers [${typeof ethers}]`)
  const accountId = ethers.utils.keccak256(user + avatar.substring(2))
  return `
    query {
      account ( id: "${accountId}" ) {
        address
        proposals {
          proposalId
          decision
        }
        stakes {
          prediction
          stakeAmount
          proposal {
            proposalId
          }
        }
        votes {
          voteOption
          proposal {
            proposalId
          }
        }
      }
    }
  `
}

export default q
