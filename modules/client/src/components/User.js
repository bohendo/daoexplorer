import React from 'react';

class User extends React.Component {
    render() {
        const { user } = this.props
        return (
            <div>
                <div className="row">
                    <div className="col-9">
                        {user.address}
                    </div>
                    <div className="col-3">
                        <button onClick={() => this.props.viewUser(user.address)}>
                            Details
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export { User }

