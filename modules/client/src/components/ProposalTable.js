import React from 'react';
import { Proposal } from './Proposal';

class ProposalTable extends React.Component {
    render(){
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col">
                        <h2>Proposals</h2>
                    </div>
                </div>
                {this.props.rowdata.map( proposal => (
                    <div key={proposal.proposalId}>
                        <Proposal proposal={proposal} viewProposal={this.props.viewProposal} alchemyData={this.props.alchemyData} />
                    </div>
                ))}
            </div>
        )
    }
}

export { ProposalTable }
