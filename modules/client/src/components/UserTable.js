import React from 'react';
import { User } from './User';

class UserTable extends React.Component {
    render() { 
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col">
                        <h2>List of Users</h2>
                    </div>
                </div>
                {this.props.rowdata.map( user => (
                    <div key={user.address}>
                        <User user={user} viewUser={this.props.viewUser}/>
                    </div>
                ))}
            </div>
        )
    }
}

export { UserTable }
