import React from 'react';

class VoteTable extends React.Component {
    render() {
        const { vote, viewProposal, alchemyData } = this.props
        //console.log(JSON.stringify(vote, null, 2))
        if (!vote) return null
        return (
            <div>
                <div key={vote.proposal.proposalId} className="row">
                    <div className="col-7">
                        <a href={alchemyData[vote.proposal.proposalId].description}>{alchemyData[vote.proposal.proposalId].title}</a>
                    </div>
                    <div className="col-3">
                        {vote.voteOption === "1" ? <p> For </p> : <p> Against </p>}
                    </div>
                    <div className="col-2">
                        <button onClick={() => viewProposal(vote.proposal.proposalId)}>
                            Info
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export { VoteTable }
