import React from 'react';
import { ProposalDetails } from './ProposalDetails';

class Proposal extends React.Component {
    render() {
        const { proposal, alchemyData, viewProposal } = this.props
        console.log(`proposal = ${JSON.stringify(this.props.proposal)}`)
        if ( !alchemyData[proposal.proposalId] ) return null

        return (
            <div>
                <div className="row">
                    <div className="col-7">
                        <a href={alchemyData[proposal.proposalId].description}>{alchemyData[proposal.proposalId].title}</a>
                    </div>
                    <div className="col-3">
                        {proposal.decision === "1" ? <p> Passed </p> : (proposal.decision === "2" ? <p> Failed </p> : <p> Awaiting </p>)}
                    </div>
                    <div className="col-2">
                        <button onClick={() => viewProposal(proposal.proposalId)}>Details</button>
                    </div>
                </div>
            </div>
        )
    }
}

export { Proposal }
