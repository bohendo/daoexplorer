import React from 'react';
import { StakerTable } from './StakerTable'
import { VoterTable } from './VoterTable'

class ProposalDetails extends React.Component {
    render() {
        const { proposal, viewUser, alchemyData } = this.props
        return (
            <div>
                <h2> Proposal: <a href={alchemyData[proposal.proposalId].description}>{alchemyData[proposal.proposalId].title}</a> </h2> 
                <br />
                <h4> Created By: <a href="#" onClick={() => viewUser(proposal.proposer.address)}>{proposal.proposer.address}</a> </h4>
                <br />
                <p> Decision: {proposal.decision === "1" ? <p> Passed </p> : (proposal.decision === "2" ? <p> Failed </p> : <p> Awaiting </p>)} </p>
                <br />
                <p>Stakers</p>
                {proposal.stakes.map( stake => (<StakerTable stake={stake} viewUser={viewUser} alchemyData={alchemyData} />))}
                <br />
                <p>Voters</p>
                {proposal.votes.map( vote => (<VoterTable vote={vote} viewUser={viewUser} alchemyData={alchemyData} />))}
            </div>
        )
    }
}

export { ProposalDetails}
