import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import './App.css';
import q from './Query';
import { ProposalDetails } from './components/ProposalDetails';
import { ProposalTable } from './components/ProposalTable';
import { UserDetails } from './components/UserDetails';
import { UserTable } from './components/UserTable';

const avatar = "0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1"
const proposals_url = 'https://daostack-alchemy.herokuapp.com/api/proposals'

//const graph_url = '/api/graph'
const graph_url = 'https://alchemy.bohendo.com/api/graph';

class App extends Component {
    constructor(props) {
        super(props)
        this.display = this.display.bind(this)
        this.viewProposal = this.viewProposal.bind(this)
        this.viewUser = this.viewUser.bind(this)
        this.state = {
            error: null,
            proposals: null,
            users: null,
            proposal: null,
            user: null,
            type: "",
            mode: "Welcome",
            alchemyData: null
        }
    }

    async execute (query, variables) {
        try{
            const response = await axios.post(graph_url,
                {
                  query,
                  variables,
                  crossdomain: true,
                  headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                  }
                },
            );
            console.log(`executed query, got reponse: ${JSON.stringify(response, null, 2)}`);
            return response.data.data;
        } catch (error) { this.setState(() => ({ error })) }
    }

    async viewProposal(id) {
        const result = await this.execute(q.getDetailsOfProposal(id));
        console.log(`viewProposal(${id}) = ${JSON.stringify(result, null, 2)}`)
        this.setState({
            mode: "ProposalDetails",
            proposal: result.proposal
        })
    }

    async viewProposals() {
        if (this.state.proposals) {
            this.setState({ mode : "Proposals" })
            return null;
        }
        const result = await this.execute(q.getAllProposalsFor(avatar), {});
        console.log(`viewProposals result = ${JSON.stringify(result)}`)
        this.setState(() => ({
            mode: "Proposals",
            proposals: result.dao.proposals
        }));
    }

    async viewUser(user) {
        console.log(`viewUser(${user})`);
        const result = await this.execute(q.getAllDetailsForUser(user, avatar), {});
        console.log(`${JSON.stringify(result, null, 2)}`);
        this.setState({
            mode: "UserDetails",
            user: result.account
        })
    }

    async viewUsers() {
        if (this.state.users) {
            this.setState({ mode : "Users" })
            return null;
        }
        const result = await this.execute(q.getAllUsers(avatar), {});
        console.log(JSON.stringify(result, null, 2));

        this.setState(() => ({
            mode: "Users",
            users: result.dao.accounts
        }));
    }

    async componentDidMount() {
        try {
            const result = await axios.get(proposals_url);
            this.setState(() => ({
                alchemyData: _.keyBy(result.data, "arcId")
            }))
            console.log(JSON.stringify(this.state.alchemyData, null, 2))
        } catch (error) {
            this.setState(() => ({error}))}
    }

    display() {
        if (this.state.mode  === "Proposals") {
            return <ProposalTable rowdata={this.state.proposals} viewProposal={this.viewProposal} alchemyData={this.state.alchemyData} />
        } else if (this.state.mode  === "Users") {
            return <UserTable rowdata={this.state.users} viewUser={this.viewUser} alchemyData={this.state.alchemyData} />
        } else if (this.state.mode === "ProposalDetails") {
            return <ProposalDetails proposal={this.state.proposal} viewProposal={this.viewProposal} viewUser={this.viewUser} alchemyData={this.state.alchemyData} />
        } else if (this.state.mode === "UserDetails") {
            return <UserDetails user={this.state.user} viewProposal={this.viewProposal} viewUser={this.viewUser} alchemyData={this.state.alchemyData} />
        }
    }

    render() {
        return (
            <div className="App">
                <button onClick={() => this.viewProposals()}>
                    Explore Proposals
                </button>
                <button onClick={() => this.viewUsers()}>
                    Explore Users
                </button>
                { this.state.mode === "Welcome" ?  <h2>Make your selection</h2> : this.display()}
            </div>
        );
    }
}


export default App;
