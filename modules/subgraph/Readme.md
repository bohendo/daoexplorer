# Information available

## Account: All the members of the DAO
    - accountID : Hash(address + DAO avatar)
    - address
    - daoAvatarAddress
    - reputation: need to debug RPC call fail
    - stakes: all stakes made by this account
    - votes: all votes made by this account
    - proposals: all proposals made by this account
    - redemptions: all the rewards redeemed by this account

## DAO: All the DAOs created using DaoCreator
    - avatar
    - controller Address
    - members: all the accounts who have rep, voted, staked or proposed in dao so far. Should I change it to only rep holders?
    - proposals
    - reputation count: having issues w RPC call fail debug 
    - reputation address

## Proposal: All proposals for a given DAO
    - submitted Time
    - proposal Id
    - proposer
    - beneficiary
    - avatar
    - reputation change
    - contribution description hash
    - external token address
    - state: need debug uint8 conversion issue
    - decision
    - execution time
    - redemptions: All redemptions for this contract
    - votes
    - stakes

# Contracts being tracked

## DaoCreater
    - NewOrg

## Reputation
    - Mint
    - Burn

## UController
    - MintReputation ( to add to total rep of DAO, can be done in Reputation mapping too but better here to get all the DAOs)
    - BurnReputation

## ContributionReward
    - RedeemReputation
    - RedeemEther
    - RedeemNativeToken
    - RedeemExternalToken

# Information currently not available

##Account Entity
    - current tokens

## DAO Entity
    - GEN balance
    - ETH balance
    - Token Count, Address, Name & Symbol
    - Name
    - Promoted Amount (How do I get that)
    - Boosted Threshold

## Proposal
    - Boosted Time (How?)
    - Boosted Period Limit
    - Pre boosted period limit

# Contracts not being tracked so far but might be useful

## GlobalContraintRegistrar 
    - NewGlobalConstraintsProposal
    - RemoveGlobalConstraintsProposal

## SchemeRegistrar
    - NewSchemeProposal
    - RemoveSchemeProposal

## UpgradeScheme
    - NewUpgradeProposal
    - RemoveUpgradeProposal

setU256
u8

Value.fromu8 -> UINT,u64 -> 
