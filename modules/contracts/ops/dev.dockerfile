FROM node:10-alpine

RUN mkdir -p /app
WORKDIR /app

# Install ganache & truffle
RUN apk add --update --no-cache bash curl g++ gcc git jq make python yarn
RUN yarn global add ganache-cli truffle

# Install node_modules
COPY package.json package.json
RUN yarn install

COPY ops/entry.sh entry.sh
COPY truffle.js truffle.js
COPY test test
COPY migrations migrations
COPY contracts contracts
COPY build build

ENTRYPOINT ["bash", "entry.sh"]
