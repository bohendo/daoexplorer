#!/usr/bin/env bash

####################
# ENV VARS

DOMAINNAME=localhost
ETH_PROVIDER="http://ethprovider:8545"
ETH_NETWORK_ID="4447"
ETH_MNEMONIC="candy maple cake sugar pudding cream honey rich smooth crumble sweet treat"

####################

project=daoexplorer
proxy_image=${project}_proxy:dev
graph_image=${project}_graph:dev
client_image=explorer_builder:dev
ethprovider_image=${project}_ethprovider:dev
database_image=postgres:10
ipfs_image=ipfs/go-ipfs:latest

function pull_if_unavailable {
    if [[ -z "`docker image ls | grep ${1%:*} | grep ${1#*:}`" ]]
    then
        docker pull $1
    fi
}

pull_if_unavailable $database_image
pull_if_unavailable $ipfs_image

function new_secret {
    secret=$2
    if [[ -z "$secret" ]]
    then
        secret=`head -c 32 /dev/urandom | xxd -plain -c 32 | tr -d '\n\r'`
    fi
    if [[ -z "`docker secret ls -f name=$1 | grep -w $1`" ]]
    then
        id=`echo $secret | tr -d '\n\r' | docker secret create $1 -`
        echo "Created secret called $1 with id $id"
    fi
}

new_secret database_graph_dev

mkdir -p /tmp/$project
cat - > /tmp/$project/docker-compose.yml <<EOF
version: '3.4'

secrets:
  database_graph_dev:
    external: true

volumes:
  chain_dev:
  graph_dev:
  ipfs_dev:
  letsencrypt_dev:

services:
  proxy:
    image: $proxy_image
    volumes:
      - letsencrypt_dev:/etc/letsencrypt
    environment:
      - MODE=dev
      - DOMAINNAME=$DOMAINNAME
    ports:
      - "80:80"
      - "443:443"

  client:
    image: $client_image
    entrypoint: node
    command: ops/start.js
    environment:
      NODE_ENV: development
      CACHE_SERVER_URL: https://localhost/api/cache
      API_URL: https://localhost/api/data
    ports:
      - "3000:3000"
    volumes:
      - `pwd`/modules/client/build:/app/build
      - `pwd`/modules/client/src:/app/src
      - `pwd`/modules/client/ops:/app/ops
      - `pwd`/modules/client/public:/app/public

  graph:
    image: $graph_image
    environment:
      network_id: $ETH_NETWORK_ID
      postgres_host: graph_db:5432
      postgres_user: $project
      postgres_db: $project
      postgres_pass_file: /run/secrets/database_graph_dev
      ipfs: ipfs:5001
      ethereum: dev:$ETH_PROVIDER
    ports:
      - "8000:8000"
      - "8001:8001"
      - "8020:8020"
    secrets:
      - database_graph_dev
    volumes:
      - `pwd`/modules/contracts/build:/contracts/build

  graph_db:
    image: $database_image
    environment:
      POSTGRES_USER: $project
      POSTGRES_DB: $project
      POSTGRES_PASSWORD_FILE: /run/secrets/database_graph_dev
    secrets:
      - database_graph_dev
    volumes:
      - graph_dev:/var/lib/postgresql/data

  ethprovider:
    image: $ethprovider_image
    environment:
      ETH_NETWORK_ID: $ETH_NETWORK_ID
      ETH_MNEMONIC: $ETH_MNEMONIC
    ports:
      - "8545:8545"
    volumes:
      - chain_dev:/data
      - `pwd`/modules/contracts/contracts:/app/contracts
      - `pwd`/modules/contracts/migrations:/app/migrations
      - `pwd`/modules/contracts/build:/app/build

  ipfs:
    image: $ipfs_image
    volumes:
      - ipfs_dev:/data/ipfs
EOF

docker stack deploy -c /tmp/$project/docker-compose.yml $project
rm -rf /tmp/$project

echo -n "Waiting for the $project stack to wake up."
while true
do
    num_awake="`docker container ls | grep $project | wc -l | sed 's/ //g'`"
    sleep 3
    if [[ "$num_awake" == "6" ]]
    then break
    else echo -n "."
    fi
done
echo " Good Morning!"
sleep 2
