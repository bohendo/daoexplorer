#!/usr/bin/env bash

project=daoexplorer

service=${project}_graph_db

container=`for f in $(docker service ps -q $service)
do
  docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' $f
done | head -n1`

if [[ -z "$1" ]]
then
    docker exec -it $container bash -c "psql $project --username=$project"
else
    docker exec -it $container bash -c "psql $project --username=$project --command=\"$1\""
fi
