# get absolute paths to important dirs
cwd=$(shell pwd)
client=$(cwd)/modules/client
contracts=$(cwd)/modules/contracts
subgraph=$(cwd)/modules/subgraph

# Use the version number from package.json to tag prod docker images
version=$(shell cat package.json | grep "\"version\":" | grep -o "[0-9.]\+")
net_version=4447

# Special variable: tells make here we should look for prerequisites
VPATH=ops:build:$(client)/build:$(contracts)/build:$(subgraph)/build

find_options=-type f -not -path "*/node_modules/*" -not -name "*.swp"
client_src=$(shell find $(client)/src $(client)/ops $(find_options))
contract_src=$(shell find $(contracts)/contracts $(contracts)/migrations $(contracts)/ops $(find_options))
subgraph_src=$(shell find $(subgraph)/src $(subgraph)/ops $(find_options))
proxy_src=$(shell find modules/proxy $(find_options))

app=daoexplorer
client_image=$(app)_client
ethprovider_image=$(app)_ethprovider
graph_image=$(app)_graph
proxy_image=$(app)_proxy

# Make sure these directories exist
$(shell mkdir -p build $(client)/build $(contracts)/build $(subgraph)/build)
me=$(shell whoami)

webpack=./node_modules/.bin/webpack
graph_cli=./node_modules/.bin/graph
truffle=./node_modules/.bin/truffle

builder_options=--name=buidler --tty --rm
	
####################
# Begin phony rules
.PHONY: default all dev prod clean

default: dev
all: dev prod
dev: proxy-dev-image ethprovider-dev-image graph-dev-image
prod: proxy-prod-image graph-prod-image
clean:
	rm -rf build
	rm -rf $(client)/build
	rm -rf $(subgraph)/build

purge: clean
	bash ops/stop.sh
	rm -rf $(contracts)/build
	docker container stop ipfs 2> /dev/null || true
	docker container prune --force
	docker volume rm `docker volume ls -q | grep "[0-9a-f]\{64\}" | tr '\n' ' '` 2> /dev/null || true
	docker volume rm --force daoexplorer_chain_dev 2> /dev/null
	docker volume rm --force daoexplorer_graph_dev 2> /dev/null
	docker volume rm --force daoexplorer_ipfs_dev 2> /dev/null

tags: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/daoexplorer/proxy:latest
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/daoexplorer/graph:latest

deploy: tags
	docker push registry.gitlab.com/$(me)/daoexplorer/proxy:latest
	docker push registry.gitlab.com/$(me)/daoexplorer/graph:latest

tags-live: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/daoexplorer/proxy:$(version)
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/daoexplorer/graph:$(version)

deploy-live: tags-live
	docker push registry.gitlab.com/$(me)/daoexplorer/proxy:$(version)
	docker push registry.gitlab.com/$(me)/daoexplorer/graph:$(version)

####################
# Begin Real Rules

# Proxy

proxy-prod-image: $(proxy_src) explorer.js
	docker build --file modules/proxy/prod.dockerfile --tag $(proxy_image):latest .
	@touch build/proxy-prod-image

proxy-dev-image: $(proxy_src) explorer.js
	docker build --file modules/proxy/dev.dockerfile --tag $(proxy_image):dev .
	@touch build/proxy-dev-image

# Client

explorer.js: explorer-builder $(client_src) $(client)/ops/dockerfile $(client)/ops/webpack.config.dev.js
	docker run $(builder_options) \
	  --volume=$(client)/src:/app/src \
	  --volume=$(client)/ops:/app/ops \
	  --volume=$(client)/public:/app/public \
	  --volume=$(client)/build:/app/build \
	  --entrypoint=yarn explorer_builder:dev build
	@touch build/explorer.js

explorer-builder: ops/builder.dockerfile $(client)/package.json
	docker build --file ops/builder.dockerfile --tag explorer_builder:dev $(client)
	@touch build/explorer-builder

# Graph node & subgraph

graph-prod-image: subgraph-prod $(subgraph)/ops/build.sh $(subgraph)/ops/entry.sh
	docker build --file $(subgraph)/ops/graph.dockerfile --tag $(graph_image):latest $(subgraph)
	@touch build/graph-prod-image

graph-dev-image: subgraph-dev $(subgraph)/ops/build.sh $(subgraph)/ops/entry.sh
	docker build --file $(subgraph)/ops/graph.dockerfile --tag $(graph_image):dev $(subgraph)
	@touch build/graph-dev-image

subgraph-prod: contract-artifacts graph-builder $(subgraph_src)
	docker network create ipfs 2> /dev/null || true
	docker run --name ipfs --network=ipfs --rm --detach --publish=5001:5001 ipfs/go-ipfs 2> /dev/null || true
	docker run $(builder_options) \
	  --volume=$(contracts)/build:/contracts/build \
	  --volume=$(subgraph)/src:/app/src \
	  --volume=$(subgraph)/ops:/app/ops \
	  --volume=$(subgraph)/build:/app/build \
	  --volume=$(subgraph)/tsconfig.json:/app/tsconfig.json \
	  --network=ipfs --entrypoint=bash \
    graph_builder:dev ops/build.sh 1
	docker container stop ipfs 2> /dev/null || true
	@touch build/subgraph-prod

subgraph-dev: contract-artifacts graph-builder $(subgraph_src)
	docker network create ipfs 2> /dev/null || true
	docker run --name ipfs --network=ipfs --rm --detach --publish=5001:5001 ipfs/go-ipfs 2> /dev/null || true
	docker run $(builder_options) \
	  --volume=$(contracts)/build:/contracts/build \
	  --volume=$(subgraph)/src:/app/src \
	  --volume=$(subgraph)/ops:/app/ops \
	  --volume=$(subgraph)/build:/app/build \
	  --volume=$(subgraph)/tsconfig.json:/app/tsconfig.json \
	  --network=ipfs --entrypoint=bash \
    graph_builder:dev ops/build.sh 4447
	docker container stop ipfs 2> /dev/null || true
	@touch build/subgraph-dev

graph-builder: $(subgraph)/package.json ops/builder.dockerfile
	docker build --file ops/builder.dockerfile --tag graph_builder:dev $(subgraph)
	@touch build/graph-builder

# Etherum provider: ganache + truffle 

ethprovider-dev-image: contract-artifacts $(contract_ops)
	docker build --file $(contracts)/ops/dev.dockerfile --tag $(ethprovider_image):dev $(contracts)
	@touch build/ethprovider-dev-image

contract-artifacts: contract-builder $(contract_src)
	docker run $(builder_options) \
	  --volume=$(contracts)/ops:/app/ops \
	  --volume=$(contracts)/build:/app/build \
	  --volume=$(contracts)/contracts:/app/contracts \
	  --volume=$(contracts)/migrations:/app/migrations \
	  --volume=$(contracts)/truffle.js:/app/truffle.js \
	  --entrypoint=bash contract_builder:dev ops/build.sh
	@touch build/contract-artifacts

contract-builder: ops/builder.dockerfile $(contracts)/package.json
	docker build --file ops/builder.dockerfile --tag contract_builder:dev $(contracts)
	@touch build/contract-builder
